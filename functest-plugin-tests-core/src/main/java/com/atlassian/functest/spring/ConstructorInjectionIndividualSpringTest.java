package com.atlassian.functest.spring;

import com.atlassian.functest.junit.SpringAwareJUnit4ClassRunner;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringAwareJUnit4ClassRunner.class)
public class ConstructorInjectionIndividualSpringTest {

    private final PluginAccessor pluginAccessor;

    @Autowired
    public ConstructorInjectionIndividualSpringTest(@ComponentImport PluginAccessor pluginAccessor)
    {
        this.pluginAccessor = pluginAccessor;
    }

    @Test
    public void injectionWorks() {
        assertThat(pluginAccessor, notNullValue());
    }
}
