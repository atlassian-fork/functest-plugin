package com.atlassian.functest.test;

import com.atlassian.functest.junit.SpringAwareTestCase;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SystemPropertyTest extends SpringAwareTestCase {
    /**
     * This test requires that a system property is set. If you run through
     * the UI, it *will* fail, because you didn't set the system property.
     */
    @Test
    public void testSystemProperty() {
        assertTrue(Boolean.getBoolean("functestSystemProperty"));
    }
}
