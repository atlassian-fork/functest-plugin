package com.atlassian.functest.rest;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;

public class ExceptionListAdaptorTest {

    private ExceptionListAdaptor exceptionListAdaptor = new ExceptionListAdaptor();

    @Test
    public void shouldSkipMarshallingOfListWithNotSerializableException() throws Exception {
        ArrayList<Throwable> exceptions = new ArrayList<>();
        exceptions.add(new IOException("An exception"));
        exceptions.add(new ExceptionWithNotSerializableField("Another exception"));
        String value = exceptionListAdaptor.marshal(exceptions);
        assertThat(value, notNullValue());

        ArrayList<Throwable> unmarshalledExceptions = exceptionListAdaptor.unmarshal(value);
        assertThat(unmarshalledExceptions, contains(
                allOf(instanceOf(IOException.class), hasProperty("message", equalTo("An exception"))),
                allOf(instanceOf(FunctestNonSerializableException.class), hasProperty("message", equalTo("Non-serializable exception com.atlassian.functest.rest.ExceptionListAdaptorTest$ExceptionWithNotSerializableField: Another exception")))));
    }

    private static class ExceptionWithNotSerializableField extends Exception {

        private Object notSerializable = new Object();

        public ExceptionWithNotSerializableField(String message) {
            super(message);
        }
    }
}
