package com.atlassian.functest.client;

import org.junit.Test;
import org.junit.runners.model.InitializationError;

import static org.junit.Assert.assertEquals;

public class RemoteTestRunnerConfigurationTest {

    @Test
    public void testConfigurationAnnotations() throws InitializationError {

        RemoteTestRunner remoteTestRunner = new RemoteTestRunner(DummyTestCase.class);

        assertEquals(DummyTestCase.OUTPUT_DIR, remoteTestRunner.outputDir);
        assertEquals(DummyTestCase.BASE_URL, remoteTestRunner.base);
    }

    @Test(expected = InitializationError.class)
    public void testConfigurationAnnotationsFailure() throws InitializationError {

        new RemoteTestRunner(FailingTestCase.class);
    }

    @Test
    public void testDefaultOutputDirConfiguration() throws InitializationError {

        RemoteTestRunner remoteTestRunner = new RemoteTestRunner(DefaultOutputDirTestCase.class);

        assertEquals(OutputDirConfigurationProvider.DEFAULT_OUTPUT_DIR, remoteTestRunner.outputDir);
    }
}
