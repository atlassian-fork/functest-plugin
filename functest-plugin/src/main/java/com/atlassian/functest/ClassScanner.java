package com.atlassian.functest;

import com.atlassian.functest.descriptor.JUnitModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import junit.framework.TestCase;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Named("classScanner")
public class ClassScanner {
    private final PluginAccessor pluginAccessor;
    private final BundleAccessor bundleAccessor;
    private final BundleContext bundleContext;
    private final Logger log = LoggerFactory.getLogger(ClassScanner.class);

    @Inject
    public ClassScanner(final BundleAccessor bundleAccessor,
                        @ComponentImport final PluginAccessor pluginAccessor,
                        final BundleContext bundleContext) {
        this.bundleAccessor = bundleAccessor;
        this.pluginAccessor = pluginAccessor;
        this.bundleContext = bundleContext;
    }

    public Map<JUnitModuleDescriptor, List<Class<?>>> findTestClassesByDescriptor(List<String> groups, List<String> includes, List<String> excludes) {
        Map<JUnitModuleDescriptor, List<Class<?>>> allFilteredClasses = new LinkedHashMap<JUnitModuleDescriptor, List<Class<?>>>();

        for (final JUnitModuleDescriptor desc : pluginAccessor.getEnabledModuleDescriptorsByClass(JUnitModuleDescriptor.class)) {
            if (groups == null || groups.isEmpty() || groups.contains(desc.getKey())) {
                Collection<Class<?>> classes = new HashSet<Class<?>>();
                for (String pkg : desc.getPackages()) {
                    final Collection<Class<?>> matchedClasses = bundleAccessor.getClassesMatching(getBundle(desc.getPlugin()), pkg);

                    if (desc.getExcludes() != null) {
                        classes.addAll(Collections2.filter(matchedClasses, new Predicate<Class<?>>() {
                            public boolean apply(Class<?> clazz) {
                                return !desc.getExcludes().contains(clazz.getName());
                            }
                        }));
                    } else {
                        classes.addAll(matchedClasses);
                    }
                }

                // only include classes that are not inner classes or anonymous classes.
                classes = Collections2.filter(classes, new Predicate<Class<?>>() {
                    public boolean apply(Class<?> clazz) {
                        return !clazz.isLocalClass() && !clazz.isMemberClass() && !clazz.isAnonymousClass();
                    }
                });

                // only include concrete classes have @Test methods or extend TestCase JUnit3-style.
                classes = Collections2.filter(classes, new Predicate<Class<?>>() {
                    public boolean apply(Class<?> clazz) {
                        if (Modifier.isAbstract(clazz.getModifiers())) {
                            return false;
                        }

                        if (TestCase.class.isAssignableFrom(clazz)) {
                            return true;
                        }

                        for (Method method : clazz.getMethods()) {
                            if (method.isAnnotationPresent(Test.class)) {
                                return true;
                            }
                        }
                        log.info("{} excluded from group '{}': no public @Test methods", clazz.getName(), desc.getKey());
                        return false;
                    }
                });

                log.debug("Found the following unfiltered test classes for group '" + desc.getKey() + "': " + classes);
                final List<Class<?>> filtered = new ArrayList<Class<?>>(filterClasses(includes, excludes, classes));
                Collections.sort(filtered, new Comparator<Class<?>>() {
                    public int compare(Class<?> o1, Class<?> o2) {
                        return o1.getName().compareTo(o2.getName());
                    }
                });

                log.debug("Found the following filtered test classes for group '" + desc.getKey() + "': " + filtered);
                allFilteredClasses.put(desc, filtered);
            }
        }

        return allFilteredClasses;
    }

    private BundleContext getBundle(Plugin plugin) {
        for (Bundle bundle : bundleContext.getBundles()) {
            Object pluginKey = bundle.getHeaders().get(OsgiPlugin.ATLASSIAN_PLUGIN_KEY);
            if (pluginKey != null && pluginKey.equals(plugin.getKey())) {
                return bundle.getBundleContext();
            }
        }
        throw new IllegalArgumentException("Cannot find bundle for plugin '" + plugin.getKey() + "'.  Make sure this "
                + "plugin is an OSGi plugin.");
    }

    /**
     * Does filtering similar to Ant's fileset blobs, however based on Java Regexps, and matching is performed on class names.
     * <p>
     * Essential rules:
     * <ul>
     * <li>If includes is supplied, then only classes matching these patterns will be included. All other classes will be excluded.
     * <li>Any classes matching the regexps in excludes will be excluded.
     * <li>If excludes is specified and no includes are specified then all files will be included apart from those excludes
     * </ul>
     *
     * @param includes a list of regexps to match to class names to include
     * @param excludes a list of regexps to match to class names to exclude
     * @param classes  the full list of classes found in this plugin
     * @return a filtered list, based on the list of includes/excludes regexps
     */
    private Collection<Class<?>> filterClasses(List<String> includes, List<String> excludes, Collection<Class<?>> classes) {

        final Collection<Class<?>> filtered = new ArrayList<Class<?>>(classes.size());

        if (includes != null && includes.size() > 0) {
            for (Class<?> aClass : classes) {
                for (String include : includes) {
                    if (aClass.getName().matches(include)) {

                        filtered.add(aClass);
                    }
                }
            }
        } else {
            filtered.addAll(classes);
        }

        if (excludes != null && excludes.size() > 0) {
            for (Class<?> aClass : classes) {
                for (String exclude : excludes) {
                    if (aClass.getName().matches(exclude)) {
                        filtered.remove(aClass);
                    }
                }
            }
        }
        return filtered;
    }
}
