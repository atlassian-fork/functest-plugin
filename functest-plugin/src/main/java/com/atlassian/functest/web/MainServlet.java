package com.atlassian.functest.web;

import com.atlassian.functest.ClassScanner;
import com.atlassian.functest.web.util.RendererContextBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

public class MainServlet extends AbstractFuncTestServlet {
    private final ClassScanner classScanner;

    @Inject
    public MainServlet(@ComponentImport final TemplateRenderer templateRenderer,
                       @ComponentImport final WebResourceManager webResourceManager,
                       final ClassScanner classScanner) {
        super(templateRenderer, webResourceManager);
        this.classScanner = classScanner;
    }

    @Override
    protected List<String> getRequiredWebResources() {
        return asList("com.atlassian.auiplugin:ajs", "com.atlassian.functest.functest-plugin:mainjs");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        List<String> includes = parseList(req, "includes");
        List<String> excludes = parseList(req, "excludes");
        List<String> groups = parseList(req, "groups");
        render("templates/main.vm",
                new RendererContextBuilder()
                        .put("contextPath", req.getContextPath())
                        .put("groups", classScanner.findTestClassesByDescriptor(groups, includes, excludes))
                        .build(),
                resp);
    }

    private List<String> parseList(HttpServletRequest req, String key) {
        String val = req.getParameter(key);
        if (val != null && val.length() > 0) {
            return asList(val.split("[, ]"));
        }
        return emptyList();
    }
}
