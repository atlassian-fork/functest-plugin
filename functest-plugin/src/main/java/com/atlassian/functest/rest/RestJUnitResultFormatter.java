package com.atlassian.functest.rest;

import junit.framework.AssertionFailedError;
import junit.framework.Test;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitResultFormatter;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitTest;
import org.apache.tools.ant.taskdefs.optional.junit.JUnitVersionHelper;

import java.io.OutputStream;

/**
 * Gathers information for the information returned via the REST resource.
 */
public class RestJUnitResultFormatter implements JUnitResultFormatter {
    private TestResult result;

    public RestJUnitResultFormatter(TestResult result) {
        this.result = result;
    }

    public void startTestSuite(JUnitTest suite) throws BuildException {
    }

    public void endTestSuite(JUnitTest suite) throws BuildException {
    }

    public void setOutput(OutputStream out) {

    }

    public void setSystemOutput(String out) {

    }

    public void setSystemError(String err) {

    }

    public void addError(Test test, Throwable t) {
        result.addError(JUnitVersionHelper.getTestCaseName(test), t);
    }

    public void addFailure(Test test, AssertionFailedError t) {
        result.addFailure(JUnitVersionHelper.getTestCaseName(test), t);
    }

    public void startTest(Test test) {
        result.addTest(JUnitVersionHelper.getTestCaseName(test));
    }

    public void endTest(Test test) {

    }
}
