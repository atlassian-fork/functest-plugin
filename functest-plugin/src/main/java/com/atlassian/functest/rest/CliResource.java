package com.atlassian.functest.rest;

import com.atlassian.annotations.security.XsrfProtectionExcluded;
import com.atlassian.functest.BundleAccessor;
import com.atlassian.functest.descriptor.JUnitModuleDescriptor;
import com.atlassian.functest.util.PluginMetaData;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.util.WaitUntil;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;

import javax.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

@Singleton
@AnonymousAllowed
@Path("/cli")
public class CliResource {
    private static final Logger LOG = PluginMetaData.instance().getLogger();
    private final PluginAccessor pluginAccessor;
    private final BundleAccessor bundleAccessor;

    @Inject
    public CliResource(@ComponentImport final PluginAccessor pluginAccessor, final BundleAccessor bundleAccessor) {
        this.pluginAccessor = pluginAccessor;
        this.bundleAccessor = bundleAccessor;
    }

    @POST
    @XsrfProtectionExcluded
    @Produces({MediaType.TEXT_PLAIN})
    public Response execute(@FormParam("group") String groupName, @FormParam("command") String command, @FormParam("cliPort") int cliPort) {
        Plugin origPlugin = null;
        for (JUnitModuleDescriptor desc : pluginAccessor.getEnabledModuleDescriptorsByClass(JUnitModuleDescriptor.class)) {
            if (desc.getKey().equals(groupName)) {
                origPlugin = desc.getPlugin();
            }
        }

        try (Socket socket = new Socket("localhost", cliPort)) {
            OutputStream out = socket.getOutputStream();
            out.write((command + "\n").getBytes());
            out.flush();
        } catch (IOException e) {
            LOG.error("Unable to send data to port", e);
            return Response.status(Response.Status.BAD_REQUEST).entity("Cannot connect to the cli port '" + cliPort + "'." +
                    "  Ensure the cli has been started (via 'atlas-cli') and the port is configured correctly on the junit module" +
                    " via the 'cliPort' attribute.").build();
        }

        final Plugin oldPlugin = origPlugin;
        WaitUntil.invoke(new WaitUntil.WaitCondition() {
            public boolean isFinished() {
                return (oldPlugin != pluginAccessor.getEnabledPlugin(oldPlugin.getKey()) &&
                        bundleAccessor.getApplicationContextForPlugin(oldPlugin.getKey()) != null);
            }

            public String getWaitMessage() {
                return "Waiting for new plugin to be installed";
            }
        }, 5, TimeUnit.SECONDS, 1);

        if (oldPlugin != pluginAccessor.getEnabledPlugin(oldPlugin.getKey())) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Timeout waiting for plugin upgrade").build();
        }
    }
}
