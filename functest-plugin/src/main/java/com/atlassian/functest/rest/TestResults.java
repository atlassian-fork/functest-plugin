package com.atlassian.functest.rest;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;

@XmlRootElement(name = "results")
public class TestResults {

    /**
     * The aggregate result of the test run {@link org.apache.tools.ant.taskdefs.optional.junit.JUnitTestRunner}
     */
    @XmlAttribute
    public int result;

    /**
     * The location the JUnit result files were written
     */
    @XmlAttribute
    public String outdir;

    /**
     * List of test names and their status
     */
    public HashMap<String, TestResult> results = new HashMap<>();

    /**
     * The plain-text formatted output
     */
    @XmlElement
    public String output;

    public TestResults() {
    }

    public TestResults(int result, String output, String outdir) {
        this.outdir = outdir;
        this.output = output;
        this.result = result;
    }

    public TestResults(String outdir) {
        this.outdir = outdir;
    }

    public void setOutdir(String outdir) {
        this.outdir = outdir;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public void addResult(String testName, TestResult testResult) {
        results.put(testName, testResult);
        result = testResult.getStatus() | result;
    }
}