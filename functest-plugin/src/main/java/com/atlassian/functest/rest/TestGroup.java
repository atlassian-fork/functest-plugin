package com.atlassian.functest.rest;

import com.google.common.base.Preconditions;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes group of junit tests.
 * <p> Usually group of tests is a single junit module descriptor installed by some plugin
 *
 * @author pandronov
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TestGroup {
    /**
     * Name of group, use as a unique group identifier
     */
    @XmlElement
    private String name = "";

    /**
     * List of tests in the group
     */
    @XmlElement
    private List<TestDescription> tests = new ArrayList<TestDescription>();

    /**
     * Set group name
     *
     * @param name new name
     * @throws NullPointerException if name is null
     */
    public void setName(@NotNull String name) {
        Preconditions.checkNotNull(name);
        this.name = name;
    }

    /**
     * Return group name
     *
     * @return group name
     */
    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    public List<TestDescription> getTests() {
        return tests;
    }

    /**
     * Adds new test into the group
     *
     * @param dsk
     */
    public void add(@NotNull TestDescription dsk) {
        this.tests.add(dsk);
    }
}
