package com.atlassian.functest.client;

import org.junit.runners.model.TestClass;

import java.lang.annotation.Annotation;

class BaseURLConfigurationProvider extends BaseConfigurationProvider<RemoteTestRunner.BaseURL> {

    BaseURLConfigurationProvider(TestClass testClass) {
        super(testClass, RemoteTestRunner.BaseURL.class);
    }

    @Override
    protected String getAnnotationValue(Annotation a) {
        return ((RemoteTestRunner.BaseURL)a).value();
    }

    @Override
    protected String getFallbackValue() {
        return "";
    }
}
