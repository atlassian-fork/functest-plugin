package com.atlassian.functest.client;

interface ConfigurationProvider<T> {

    T getValue();
}
